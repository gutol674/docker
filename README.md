### Setup do projeto

1. Fazer o clone do projeto 'backend', 'frontend' e 'docker'.
2. No arquivo docker-compose.yml, mudar os caminhos que possuem "/home/user2" para o caminho do seu projeto local.
3. Rodar 'docker-compose build'.
4. Rodar o composer install no projeto 'backend'.
5. Configurar o .env do projeto 'backend' com a configuração de conexão ao seu banco local.
6. Configurar seu virtualhost caso necessário /etc/hosts

### Testando o projeto

Foram aplicadas migrations e seeders pelo projeto backend, obs: as migrations são NECESSÁRIAS para que a aplicação funcione corretamente.

SwaggerUI: Foi implementado uma OpenAPI pelo projeto backend, e uma interface SwaggerUI pelo projeto frontend, para acessar é necessário rodar o comando "npm run start" dentro do projeto 'frontend'.

Pelo proprio frontend, é possível testar a aplicação rodando o comando "npm run serve", e navegando pela interface.

### Code Quality

Foram desenvolvidos poucos testes unitários, menos do que eu gostaria, mas por falta de tempo pela minha parte, fiz apenas para mostrar minha capacidade em desenvolve-los, para rodar os testes unitário rode o comando 'vendor/bin/phpunit' dentro do projeto 'backend'

O Projeto backend foi desenvolvido com phpcs usando PSR1

Foi adicionado um pipeline via gitlab CI para a validação dos commits, o CI pode ser consultado diretamente no repositório 'backend'.

### Diagrama de Classes

![](projeto.png)

### Considerações finais

Algumas coisas não ficaram exatamente como eu gostaria, eu queria explorar mais a parte do CI, fazer um projeto frontend melhor, entre outros detalhes, mas infelizmente eu não tive o tempo livre necessário.

Pode ser que eu esqueci alguma especificação, mas podem me procurar para o esclarecimento de qualquer coisa.

Obrigado!